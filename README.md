# Keyword Scanner
Keyword scanner scans through all the files in the specified directory, flagging out files that contains the specified keyword.

# Code style
[![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

# Installation
This is a Node.js application hence, please ensure Node.js is installed.
Test cases for keyword scanner are written with Mocha. To install the dependencies, run `npm install` in the working directory.

*Visit https://nodejs.org/en/ for the latest version of Node.js.*

# Tests
To run the test cases, simply run `npm run test` in the working directory after installing the dependencies.

# Usage
### Searching TODOs
1. Copy app.js and keywordscanner.js into the directory that you want to search for `"TODO"`.
2. Run `node app.js` in the cmdline to begin the search.
3. The output will be the list of all files (and their absolute paths) that contains the keyword `"TODO"` in them.

Sample output:
```
$ node app.js
/home/admin/keywordscanner/node_modules/@eslint/eslintrc/node_modules/js-yaml/dist/js-yaml.js
/home/admin/keywordscanner/node_modules/@eslint/eslintrc/node_modules/js-yaml/lib/js-yaml/loader.js
/home/admin/keywordscanner/node_modules/@eslint/eslintrc/node_modules/js-yaml/lib/js-yaml/type.js
/home/admin/keywordscanner/node_modules/@types/json5/index.d.ts
/home/admin/keywordscanner/node_modules/ajv/dist/ajv.bundle.js
```

4. *Enhancement* Run `node app.js --show-occurrence` will list all the files including the number of `"TODO"` in each file.

Sample output:
```
$ node app.js --show-occurrence
/home/admin/keywordscanner/node_modules/@eslint/eslintrc/node_modules/js-yaml/dist/js-yaml.js - 2 TODO(s)
/home/admin/keywordscanner/node_modules/@eslint/eslintrc/node_modules/js-yaml/lib/js-yaml/loader.js - 1 TODO(s)
/home/admin/keywordscanner/node_modules/@eslint/eslintrc/node_modules/js-yaml/lib/js-yaml/type.js - 1 TODO(s)
/home/admin/keywordscanner/node_modules/@types/json5/index.d.ts - 1 TODO(s)
/home/admin/keywordscanner/node_modules/ajv/dist/ajv.bundle.js - 1 TODO(s)
```

The core functionalities are in keywordscanner.js, app.js is just a simple app that utilize the `KeywordScanner` class to perform the search.

# KeywordScanner
`KeywordScanner` class can be imported into your Node.js applications to utilize its functionalities.
To use, simply instantiate a new `KeywordScanner` class and call its respective methods.

```
const KeywordScanner = require('./keywordscanner.js')
const keywordScan = new KeywordScanner()
keywordScan.filesWith(keyword, dir, showOccurrence)
```
## Instance methods

### 1. `keywordScan.sanitizeDir (dir)`
Sanitize directory input string by replacing back slash with forward slash and trimming off whitespaces.

#### Parameters:
- `dir`: `string`

#### Return value:
- A string with all back slash replaced with forward slash and no trailing whitespaces.
<br/>

### 2. `keywordScan.readDirectory (dir)`
Reads the immediate directory and sub-directories for all the files and their absolute paths.

#### Parameters:
- `dir`: `string`

#### Return value:
- A string array containing all the files and their absolute paths.
- `undefined` if the directory does not exist.
<br/>

### 3. `keywordScan.scanFor (keyword, filename, showOccurrence)`
Scans the specified file for the keyword.

#### Parameters:
- `keyword`: `string`
- `filename`: `string`
- `showOccurrence`: `boolean`

#### Return value:
- `true` if the file contains the keyword and `showOccurrence` argument is set to `false`.
- `false` if the file does not contain the keyword.
- `number` if the file contains the keyword and `showOccurrence` argument is set to `true`.
<br/>

### 4. `keywordScan.filesWith (keyword, dir = './', showOccurrence = false)`
`async` method that finds all the files in the immediate directory and sub-directories that contains the keyword. It accepts both relative path and absolute paths as the `dir` argument.

#### Parameters:
- `keyword`: `string`
- `dir`: `string`; Default value is `./`.
- `showOccurrence`: `boolean`; Default value is `false`.

#### Return value:
- `Invalid directory` string if the directory `dir` is invalid.
- `No files with <keyword>` string if no files contains the keyword in the immediate directory and sub-directories.
- A string array containing all the files and their **absolute paths** that contains the keyword. If `showOccurrence` is set to `true`, `" - <numberOfOccurrences> <keyword>(s)"` string is appended to the end of the absolute paths.
<br/>

### 5. `keywordScan.printOutput (output)`
Console log every string element in a string array. To be used in conjuction with the `filesWith` method to print all the files containing the keyword and their absolute paths in the console.

#### Parameters:
- `output`: `string`

#### Return value:
- No return value.
<br/>

# Assumptions
Files to be scanned are assumed to be UTF-8 encoded.