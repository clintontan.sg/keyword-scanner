const KeywordScanner = require('./keywordscanner.js')

const keywordScan = new KeywordScanner()

const args = process.argv.slice(2)
const showOccurrenceFlag = '--show-occurrence'
const dir = './'
const keyword = 'TODO'
let showOccurrence = false

if (args.includes(showOccurrenceFlag)) showOccurrence = true

keywordScan.filesWith(keyword, dir, showOccurrence).then(result => keywordScan.printOutput(result))
