const assert = require('assert')

const KeywordScanner = require('../keywordscanner.js')

describe('Keyword Scanner', () => {
  const keywordScan = new KeywordScanner()
  const fs = require('fs')

  it('should sanitize directory input by replacing back slash with forward slash', () => {
    let dir = '\\test\\'
    assert.strictEqual(true, dir.includes('\\'))
    dir = keywordScan.sanitizeDir(dir)
    assert.strictEqual(false, dir.includes('\\'))
  })

  it('should return undefined when reading a directory that is undefined, null, not a string type, whitespace, non-existent', () => {
    assert.strictEqual(undefined, keywordScan.readDirectory())
    assert.strictEqual(undefined, keywordScan.readDirectory(null))
    assert.strictEqual(undefined, keywordScan.readDirectory(10))
    assert.strictEqual(undefined, keywordScan.readDirectory('10'))
    assert.strictEqual(undefined, keywordScan.readDirectory('     '))
    assert.strictEqual(undefined, keywordScan.readDirectory('/invalid'))
    assert.strictEqual(undefined, keywordScan.readDirectory('/invalid/'))
  })

  it('should return an array of files recursively when reading the directory: "./" ', () => {
    const fileArray = keywordScan.readDirectory('./')
    assert.strictEqual(typeof [], typeof fileArray)
    for (let i = 0; i < fileArray.length; i++) {
      assert.strictEqual(typeof '', typeof fileArray[i])
      assert.strictEqual(true, fs.existsSync(fileArray[i]))
    }
  })

  it('should return an array of files recursively when reading the directory without trailing forward slash: "." ', () => {
    const fileArray = keywordScan.readDirectory('.')
    assert.strictEqual(typeof [], typeof fileArray)
    for (let i = 0; i < fileArray.length; i++) {
      assert.strictEqual(typeof '', typeof fileArray[i])
      assert.strictEqual(true, fs.existsSync(fileArray[i]))
    }
  })

  it('should return true when scanning file containing "TODO". File: ./test/keywordscanner.test.js', async () => {
    assert.strictEqual(true, await keywordScan.scanFor('TODO', './test/keywordscanner.test.js'))
  })

  it('should return number of occurrences of the keyword "TODO" when scanning file containing "TODO" when showOccurrence is set to true (default: false). File: ./test/keywordscanner.test.js', async () => {
    const result = await keywordScan.scanFor('TODO', './test/keywordscanner.test.js', true) // last argument showOccurrence (default: false)
    assert.strictEqual(false, isNaN(result))
  })

  it('should return false when scanning file not containing "TODO". File: ./package.json', async () => {
    assert.strictEqual(false, await keywordScan.scanFor('TODO', './package.json'))
  })

  it('should return false when scanning file not containing "TODO" when showOccurrence is set to true (default: false). File: ./package.json', async () => {
    assert.strictEqual(false, await keywordScan.scanFor('TODO', './package.json', true)) // last argument showOccurrence (default: false)
  })

  it('should return "Invalid keyword" when keyword is null, undefined, or whitespace', async () => {
    assert.strictEqual('Invalid keyword', await keywordScan.filesWith())
    assert.strictEqual('Invalid keyword', await keywordScan.filesWith(null))
    assert.strictEqual('Invalid keyword', await keywordScan.filesWith('     '))
  })

  it('should return "Invalid directory" when directory is null, whitespace, non-existent directory', async () => {
    assert.strictEqual('Invalid directory', await keywordScan.filesWith('TODO', null))
    assert.strictEqual('Invalid directory', await keywordScan.filesWith('TODO', '   '))
    assert.strictEqual('Invalid directory', await keywordScan.filesWith('TODO', './invalid'))
    assert.strictEqual('Invalid directory', await keywordScan.filesWith('TODO', './invalid/'))
    assert.strictEqual('Invalid directory', await keywordScan.filesWith('TODO', 'abc'))
  })

  it('should return files containing keyword "TODO" when scanning the directory "."', async function () {
    this.timeout(5000)
    const keyword = 'TODO'
    const dir = '.'
    const todoFiles = await keywordScan.filesWith(keyword, dir)
    for (let i = 0; i < todoFiles.length; i++) {
      const result = await keywordScan.scanFor(keyword, todoFiles[i])
      assert.strictEqual(true, result)
    }
  })

  it('should return files containing keyword "TODO" and number of occurrences when scanning the directory ".", when showOccurrence is set to true (default: false).', async function () {
    this.timeout(5000)
    const keyword = 'TODO'
    const dir = '.'
    const todoFiles = await keywordScan.filesWith(keyword, dir, true) // last argument showOccurrence (default: false)
    for (let i = 0; i < todoFiles.length; i++) {
      assert.strictEqual(true, todoFiles[i].includes(` ${keyword}(s)`))
    }
  })

  it('should return the string "No files with "TODO"." when scanning the directory for files without "TODO". Directory: "./no todos test folder"', async () => {
    const keyword = 'TODO'
    const dir = './no todos test folder'
    const todoFiles = await keywordScan.filesWith(keyword, dir)
    assert.strictEqual(`No files with "${keyword}".`, todoFiles)
  })

  it('should return the string "No files with "TODO"." when scanning the directory for files without "TODO" even when showOccurrence is set to true (default: false). Directory: "./no todos test folder"', async () => {
    const keyword = 'TODO'
    const dir = './no todos test folder'
    const todoFiles = await keywordScan.filesWith(keyword, dir, true) // last argument showOccurrence (default: false)
    assert.strictEqual(`No files with "${keyword}".`, todoFiles)
  })
})
