User Stories:

1. User wants to list all absolute paths containing the files that contains "TODO" in the current directory that the application is in.
    - directory might contain sub folders
    - files containing TODO might be in sub folders (recursive)
2. User wants to view the result in lines of absolute paths of the files that contains "TODO".
3. User wants to list the number of TODOs in each file at the end of the file path. (Enhancement)

Assumptions:
- Files to be scanned are UTF8 encoded.