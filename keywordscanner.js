const fs = require('fs')
const resolve = require('path').resolve
class KeywordScanner {
  sanitizeDir (dir) {
    dir = dir.trim()
    dir = dir.replace(/\\/g, '/')
    return dir
  }

  readDirectory (dir) {
    if (dir === undefined || dir === null || typeof dir !== 'string' || dir.trim() === '') return undefined
    dir = this.sanitizeDir(dir)
    if (fs.existsSync(dir)) {
      let filePathArray = []
      if (fs.lstatSync(dir).isDirectory()) {
        if (!dir.match(/\/$/)) dir = dir + '/'
        const fileNamesArray = fs.readdirSync(dir, { withFileTypes: true })
        fileNamesArray.forEach(file => {
          if (!file.isDirectory()) {
            filePathArray.push(resolve(dir + file.name))
          } else {
            const tempArray = this.readDirectory(dir + file.name + '/')
            filePathArray = filePathArray.concat(tempArray)
          }
        })
      } else {
        filePathArray.push(dir)
      }
      return filePathArray
    }
    return undefined
  }

  scanFor (keyword, filename, showOccurrence) {
    return new Promise((resolve) => {
      if (fs.existsSync(filename)) {
        fs.readFile(filename, 'utf8', (err, data) => {
          if (err) throw err
          if (data.includes(keyword)) {
            showOccurrence ? resolve(data.split(keyword).length - 1) : resolve(true)
          }
          resolve(false)
        })
      } else {
        resolve(false)
      }
    })
  }

  async filesWith (keyword, dir = './', showOccurrence = false) {
    if (keyword === undefined || keyword === null || keyword.trim() === '') return 'Invalid keyword'
    const filesWithKeyword = []
    const allFiles = this.readDirectory(dir)
    if (allFiles !== undefined) {
      for (let i = 0; i < allFiles.length; i++) {
        const result = await this.scanFor(keyword, allFiles[i], showOccurrence)
        if (result !== false) {
          showOccurrence ? filesWithKeyword.push(allFiles[i] + ` - ${result} ${keyword}(s)`) : filesWithKeyword.push(allFiles[i])
        }
      }
    } else {
      return 'Invalid directory'
    }

    if (filesWithKeyword.length === 0) {
      return `No files with "${keyword}".`
    }
    return filesWithKeyword
  }

  printOutput (output) {
    if (typeof output !== 'string') {
      for (let i = 0; i < output.length; i++) {
        console.log(output[i])
      }
    } else {
      console.log(output)
    }
  }
}

module.exports = KeywordScanner
